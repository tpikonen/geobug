# Geobug

Geobug is an adaptive client for GeoClue, the geolocation D-bus server from
freedesktop.org. It can display your location information (coordinates, speed
etc.) and save a track of your movements to a GPX-file.

## License

GPL-3.0

## Screenshots

![Main view](https://tpikonen.codeberg.page/geobug/img/screenshot-default.png)
![Expanded view](https://tpikonen.codeberg.page/geobug/img/screenshot-expanded.png)
![Landscape view, dark mode](https://tpikonen.codeberg.page/geobug/img/screenshot-landscape-dark.png)

## Dependencies:

    python 3.6+, PyGObject, GTK4, libadwaita, Geoclue, gpxpy

## Installing and running

### Flathub

<a href='https://flathub.org/apps/details/page.codeberg.tpikonen.geobug'>
<img width='240' alt='Get it on Flathub' src='https://flathub.org/api/badge?locale=en'/>
</a>

Geobug is
[in flathub](https://flathub.org/apps/details/page.codeberg.tpikonen.geobug)
and can be installed from there, or from a software manager like Gnome software.
The direct install link is
[here](https://dl.flathub.org/repo/appstream/page.codeberg.tpikonen.geobug.flatpakref).

Run

    flatpak run page.codeberg.tpikonen.geobug

to execute from the command line.

### From source tree

Run the script `bin/geobug`.

### pip / pipx install from source tree

Run

    pip install --user --break-system-packages ./

in the source tree root.

This creates an executable Python script in `$HOME/.local/bin/geobug`.

Alternatively you can install to a venv with `pipx` by running these commands
in the source tree root:

    pipx install ./
    pipx inject -r requirements.txt geobug

### Flatpak from source tree

Run

    flatpak-builder --install --user build-dir flatpak/page.codeberg.tpikonen.geobug.yaml

in the source tree root. Then run

    flatpak run page.codeberg.tpikonen.geobug

to execute.

## Hints

You can start recording a GPX track by selecting 'Record track' from the main menu. The GPX file is saved in `$HOME/Documents/geobug-tracks`.

<center>
  <img src="https://tpikonen.codeberg.page/geobug/img/page.codeberg.tpikonen.geobug.svg" alt="Geobug logo" style="width:128px;height:128px;" />
</center>
