# Copyright 2021-2024 Teemu Ikonen
# SPDX-License-Identifier: GPL-3.0-only

import os
from datetime import datetime

import gi
gi.require_version('Adw', '1')
gi.require_version('Gdk', '4.0')
gi.require_version('Geoclue', '2.0')
from gi.repository import Adw, Gdk, Geoclue  # noqa: E402


acc2text = {
    Geoclue.AccuracyLevel.COUNTRY: "Country",
    Geoclue.AccuracyLevel.CITY: "City",
    Geoclue.AccuracyLevel.NEIGHBORHOOD: "Neighborhood",
    Geoclue.AccuracyLevel.STREET: "Street",
    Geoclue.AccuracyLevel.EXACT: "Exact",
}


def have_touchscreen():
    """Return True if the default seat of default display has touch capability."""
    return bool(Gdk.Display.get_default_seat(
        Gdk.Display.get_default()).get_capabilities() & Gdk.SeatCapabilities.TOUCH)


def show_alertdialog(parent, heading, body, button):
    """Show an Adw.AlertDialog with a single button."""

    def _on_closed(dialog, res, data):
        dialog.choose_finish(res)

    dialog = Adw.AlertDialog.new(heading, body)
    dialog.add_response("close", button)
    dialog.choose(parent, None, _on_closed, None)


def unique_filename(namestem, ext, timestamp=False):
    if timestamp:
        namestem += "-" + datetime.now().isoformat(
            '_', 'seconds').replace(':', '.')
    name = None
    for count in ('~%d' % n if n > 0 else '' for n in range(100)):
        test = namestem + count + ext
        if os.path.exists(test):
            continue
        else:
            name = test
            break

    return name


def bearing_to_arrow(bearing):
    # Arrows in bearing order, with up arrow (north) repeating at the end
    arrows = [
        '\u2191',
        '\u2197',
        '\u2192',
        '\u2198',
        '\u2193',
        '\u2199',
        '\u2190',
        '\u2196',
        '\u2191',
    ]
    edges = [22.5 + 45.0 * n for n in range(0, 8)] + [360.0]

    angle = bearing - (bearing // 360) * 360
    index = next(ind for (ind, e) in enumerate(edges) if angle < e)
    return arrows[index]
