# Copyright 2021-2024 Teemu Ikonen
# SPDX-License-Identifier: GPL-3.0-only

import sys

from .application import Application


def main():
    app = Application()
    app.run()
    sys.exit(0)


if __name__ == '__main__':
    main()
