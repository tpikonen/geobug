# Copyright 2021-2024 Teemu Ikonen
# SPDX-License-Identifier: GPL-3.0-only

import argparse
import importlib.resources as resources
import os
import signal
import sys
import time
from datetime import datetime, timezone
from geobug import __version__

import gi

from .gpx import GpxSaver
from .util import acc2text, bearing_to_arrow, show_alertdialog, unique_filename
from .window import Window

gi.require_version('Geoclue', '2.0')
gi.require_version('Gtk', '4.0')
gi.require_version('Gdk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Adw, GLib, Gdk, Geoclue, Gio, Gtk  # noqa: E402, I100

appname = 'Geobug'
app_id = 'page.codeberg.tpikonen.geobug'
respath = __name__.split(sep='.')[0]

param2acc = {
    "ACC_COUNTRY": Geoclue.AccuracyLevel.COUNTRY,
    "ACC_CITY": Geoclue.AccuracyLevel.CITY,
    "ACC_NEIGHBORHOOD": Geoclue.AccuracyLevel.NEIGHBORHOOD,
    "ACC_STREET": Geoclue.AccuracyLevel.STREET,
    "ACC_EXACT": Geoclue.AccuracyLevel.EXACT,
}

acc2param = {v: k for k, v in param2acc.items()}

default_accuracy = Geoclue.AccuracyLevel.STREET


class Application(Adw.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(
            application_id=app_id,
            flags=Gio.ApplicationFlags.FLAGS_NONE)

        GLib.set_application_name(appname)
        GLib.set_prgname(app_id)

        self.window = None

        desc = "Adaptive GeoClue client"
        parser = argparse.ArgumentParser(description=desc)
        parser.add_argument(
            '-c', '--console-output', dest='console_output',
            action='store_true', default=False,
            help='Output location data to console')
        parser.add_argument(
            '-a', '--accuracy-level', dest='accuracy',
            choices=['country', 'city', 'neighborhood', 'street', 'exact'],
            default=acc2text[default_accuracy].lower(),
            help="Set initial accuracy level.")
        self.args = parser.parse_args()

        optacc2acc = {v.lower(): k for k, v in acc2text.items()}
        self.accuracy = optacc2acc[self.args.accuracy]

        GLib.unix_signal_add(GLib.PRIORITY_DEFAULT, signal.SIGINT,
                             self.sigint_handler)

        self.connect('startup', self.on_startup)
        self.connect('activate', self.on_activate)
        self.connect('shutdown', self.on_shutdown)

        # Internal state
        self.last_data = None
        self.last_update = None
        self.source = None
        self.refresh_rate = 1  # Really delay between updates in seconds
        self.location = None
        self.sigint_received = False
        self.timeout_source_id = None

        # GPX
        docdir = (GLib.get_user_special_dir(
            GLib.UserDirectory.DIRECTORY_DOCUMENTS)
            or os.path.join(GLib.get_home_dir(), 'Documents'))
        self.gpx_save_dir = os.path.join(docdir, 'geobug-tracks')
        self.gpx = None

        self.inhibit_cookie = 0

    def log_msg(self, text):
        if self.window is None:
            print(text)
        else:
            self.window.log_msg(text)

    def sigint_handler(self):
        if not self.sigint_received:
            print("Interrupt signal (Ctrl-C) received")
            self.sigint_received = True
            self.quit()
        else:
            print("Interrupt signal (Ctrl-C) received again, force exit")
            sys.exit(0)

    def create_actions(self):
        app_actions = [
            ("about", self.on_about, None),
            ("quit", self.on_quit, ("app.quit", ["<Ctrl>Q"])),
            ("record", self.on_record, ("app.record", ["<Ctrl>R"])),
            ("stop_recording", self.on_stop_recording,
             ("app.stop_recording", ["<Ctrl>S"])),
            ("copy_coordinates", self.on_copy_coordinates,
             ("app.copy_coordinates", ["<Ctrl>C"])),
        ]
        for action, callback, accel in app_actions:
            simple_action = Gio.SimpleAction.new(action, None)
            simple_action.connect('activate', callback)
            self.add_action(simple_action)
            if accel is not None:
                self.set_accels_for_action(*accel)

        action = Gio.SimpleAction.new_stateful(
            'accuracy', GLib.VariantType.new('s'),
            GLib.Variant.new_string(
                acc2param.get(self.accuracy, default_accuracy)))
        action.connect('activate', self.on_accuracy_changed)
        self.add_action(action)

    def setup_styles(self):
        provider = Gtk.CssProvider()
        provider.load_from_data(
            resources.read_binary(respath, "main.css"))
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(), provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def on_startup(self, app):
        Adw.Application.do_startup(self)
        Adw.StyleManager.get_default().set_color_scheme(
            Adw.ColorScheme.PREFER_LIGHT)
        self.create_actions()
        GLib.idle_add(self.init_source)

    def on_activate(self, app):
        if not self.window:
            self.setup_styles()
            self.window = Window(self)
            self.window.set_default_icon_name(app_id)
            self.log_msg(f"{appname} version {__version__} started")
            # Initialize dataframe
            self.set_values({}, console_output=self.args.console_output)

        self.window.present()

    def on_shutdown(self, app):
        print("Cleaning up... ", end='', flush=True)
        if self.gpx is not None:
            self.gpx.write()
        if self.timeout_source_id is not None:
            GLib.Source.remove(self.timeout_source_id)
        print("done.")

    def on_quit(self, *args):
        self.quit()

    def on_about(self, *args):
        adlg = Adw.AboutDialog(
            application_name=appname,
            application_icon=app_id,
            developer_name="Teemu Ikonen",
            version=__version__,
            website="https://codeberg.org/tpikonen/geobug/",
            comments="An adaptive GeoClue client",
            license_type=Gtk.License.GPL_3_0_ONLY,
            copyright="Copyright 2021-2024 Teemu Ikonen",
        )
        adlg.present(self.window)

    def on_record(self, *args):
        assert self.gpx is None

        namestem = self.gpx_save_dir + '/geobug'
        gpxfile = unique_filename(namestem, '.gpx', timestamp=True)
        if gpxfile is None:
            raise FileExistsError(namestem)

        self.gpx = GpxSaver(gpxfile)

        self.window.start_recording()
        self.lookup_action('record').set_enabled(False)
        self.log_msg("Started saving track to '%s'" % self.gpx.filename)

        self.inhibit_cookie = self.inhibit(
            self.window, Gtk.ApplicationInhibitFlags.SUSPEND,
            "Recording GPX track")
        if self.inhibit_cookie == 0:
            self.log_msg("Failed to inhibit system suspend")
        else:
            self.log_msg("Inhibiting system suspend")

        if self.last_data is not None:
            self.gpx.update(self.last_data)
            self.recording_label_update()

    def on_stop_recording(self, *args):
        dialog = Adw.AlertDialog.new(
            "Stop recording?",
            "Do you want to stop recording a track?"
        )
        dialog.add_response("no", "No")
        dialog.add_response("yes", "Yes")
        dialog.set_default_response("no")
        dialog.set_close_response("no")
        dialog.set_response_appearance("yes", Adw.ResponseAppearance.DESTRUCTIVE)
        dialog.choose(self.window, None, self.on_stop_recording_done, None)

    def on_stop_recording_done(self, source_object, res, data):
        dialog = source_object
        response = dialog.choose_finish(res)
        if response == "yes":
            self.gpx.write()
            self.log_msg("Track closed and saved to '%s'" % self.gpx.filename)
            self.gpx = None
            self.uninhibit(self.inhibit_cookie)
            self.window.stop_recording()
            self.lookup_action('record').set_enabled(True)
        else:
            pass

    def toast(self, text):
        label = Gtk.Label.new(text)
        label.set_use_markup(True)
        toast = Adw.Toast.new(text)
        toast.set_custom_title(label)
        self.window.toast_overlay.add_toast(toast)

    def on_copy_coordinates(self, *args):
        if self.last_data is not None:
            lat = self.last_data.get('latitude')
            lon = self.last_data.get('longitude')

        if (self.last_data is None or (lat is None and lon is None)):
            text = "<b>Coordinates are not known,\ncannot copy</b>"
        else:
            cb = Gdk.Display().get_default().get_clipboard()
            ctext = f"{lat:0.6f}, {lon:0.6f}"
            cb.set(ctext)
            text = f"<b>'{ctext}'\ncopied to clipboard</b>"

        self.toast(text)

    def on_location_notify(self, location, params, *data):
        self.on_location_changed(location)

    def on_location_proxy_ready(self, source, res, *data):
        try:
            location = Geoclue.LocationProxy.new_for_bus_finish(res)
        except Exception:
            self.log_msg("Connection to GeoClue lost")
            return

        self.location = location
        location.connect("notify::timestamp", self.on_location_notify)
        self.on_location_changed(location)

    def on_location_updated(self, client, old_location, new_location, *data):
        Geoclue.LocationProxy.new_for_bus(Gio.BusType.SYSTEM,
                                          Gio.DBusProxyFlags.NONE,
                                          "org.freedesktop.GeoClue2",
                                          new_location,
                                          None,
                                          self.on_location_proxy_ready,
                                          None)

    def on_client_active_changed(self, client, pspec, *data):
        if client.props.active:
            self.log_msg("GeoClue client is active")
        else:
            self.log_msg("Client was stopped by GeoClue")

    def on_client_start_ready(self, source, res, *data):
        Geoclue.ClientProxy.call_start_finish(self.source, res)

    def on_client_proxy_ready(self, source, res, *data):
        try:
            self.source = Geoclue.ClientProxy.create_finish(res)
            if self.source is None:
                raise Exception("ClientProxy.create returned None")
        except Exception as e:
            estr = str(e)
            dtext = "Could not connect to GeoClue" + f": {estr}" if estr else ""
            self.log_msg(f"Failed to connect to GeoClue: {dtext}")
            show_alertdialog(self.window, "GeoClue not found", dtext, "OK")
            return

        if self.source.props.requested_accuracy_level != self.accuracy.real:
            # We have changed accuracy while waiting
            self.source.props.requested_accuracy_level = self.accuracy.real

        self.source.connect("location-updated", self.on_location_updated)
        self.source.connect("notify::active", self.on_client_active_changed)
        self.source.call_start(None, self.on_client_start_ready)
        self.timeout_source_id = GLib.timeout_add(
            self.refresh_rate * 1000, self.timeout_cb, None)
        self.window.sensitive(True)
        self.log_msg("Connected to GeoClue")

    def init_source(self):
        self.log_msg("Connecting to GeoClue with accuracy '%s' ..." % (
                     acc2text[self.accuracy]))
        Geoclue.ClientProxy.create(app_id,
                                   self.accuracy,
                                   None,
                                   self.on_client_proxy_ready)
        return False  # Remove from idle_add

    def on_accuracy_changed(self, action, param):
        new_accuracy = param2acc.get(param.get_string(),
                                     default_accuracy)
        if new_accuracy == self.accuracy:
            return

        self.accuracy = new_accuracy
        self.log_msg(f"Setting accuracy to {acc2text[self.accuracy]}")
        if self.source is not None:
            self.source.call_stop()
            self.source.props.requested_accuracy_level = self.accuracy.real
            self.source.call_start()
        action.set_state(param)
        if self.window is not None:
            self.window.relabel_accuracy_menubutton(None, None)

    def set_values(self, data, console_output=True):
        utcfmt = "%H:%M:%S UTC"

        def to_str(x, fmt="%s"):
            return fmt % x if x is not None else "-"

        # Mapping: Data key, description, converter func
        order = [
            ("fixage", "Age of fix", lambda x: to_str(x, "%0.0f s")),
            ("accuracy", "Accuracy", lambda x: to_str(x, "%0.0f m")),
            ("latitude", "Latitude", lambda x: "%0.6f" % x if x else "-"),
            ("longitude", "Longitude", lambda x: "%0.6f" % x if x else "-"),
            ("altitude", "Altitude", lambda x: to_str(x, "%0.1f m")),
            ("speed", "Speed", lambda x: to_str(x, "%0.1f m/s")),
            ("heading", "Heading", lambda x: to_str(x, "%0.1f deg ")
                + (bearing_to_arrow(x) if x is not None else "")),
            ("datetime", "Time of fix", lambda x: x.strftime(utcfmt)),
            ("systime", "Sys. Time", lambda x: x.strftime(utcfmt)),
            ("description", "Description", lambda x: x if x else "-"),
        ]
        descs = []
        vals = []
        for key, desc, fun in order:
            if key not in data.keys():
                value = "n/a"
            else:
                value = fun(data[key])
            descs.append(desc)
            vals.append(value)

        if self.window is not None:
            if self.window.dataframe.rows != len(descs):
                self.window.dataframe.set_rowtitles(descs)
            self.window.dataframe.set_values(vals)

        if console_output:
            for i, val in enumerate(vals):
                print(f"{descs[i]}: {val}")
            print("--")

    def recording_label_update(self):
        tdelta = self.gpx.get_duration()
        elapsed = round(tdelta.total_seconds())
        hours, rem = divmod(elapsed, 3600)
        mins, secs = divmod(rem, 60)

        meters = self.gpx.get_length()
        dist_str = (f"{str(round(meters))} m" if meters < 1000.0 else
                    f"{(meters / 1000.0):.1f} km")

        self.window.recording_label.set_markup(
            f"<span font-features='tnum'>"
            f"Track: <b>{dist_str}, {hours:02}:{mins:02}:{secs:02}</b>"
            f"</span>")

    def timeout_cb(self, x):
        data = self.last_data
        if not data:
            return True

        now_dt = datetime.now(timezone.utc)
        data['systime'] = now_dt
        data['fixage'] = (now_dt - data['datetime']).total_seconds()

        dt = ((time.time() - self.last_update)
              if self.last_update else 3 * self.refresh_rate)
        if dt > self.refresh_rate:
            self.set_values(data, console_output=False)
        return GLib.SOURCE_CONTINUE

    def on_location_changed(self, location, *args):
        self.last_update = time.time()
        self.update(location)

    def update(self, new_location):
        if self.source is None:
            return

        keys = ('accuracy', 'altitude', 'description', 'heading', 'latitude',
                'longitude', 'speed', 'timestamp')
        data = {k: getattr(new_location.props, k) for k in keys
                if getattr(new_location.props, k, None) is not None}

        # Used by GpxSaver
        data['latlon'] = (data.get('latitude'), data.get('longitude'))

        if data.get('altitude', -1.0e6) < -1.0e5:
            data["altitude"] = None

        if data.get('speed', -1.0) < 0.0:
            data["speed"] = None

        if data.get('heading', -1.0) < 0.0:
            data["heading"] = None

        now_dt = datetime.now(timezone.utc)
        data['systime'] = now_dt

        def variant2dt(x, default=None):
            if not isinstance(x, GLib.Variant):
                return default
            sec, usec = x.unpack()
            ts = float(sec) + 1.0e-6 * float(usec)
            return datetime.fromtimestamp(ts, timezone.utc)

        fixdt = variant2dt(
            data['timestamp'],
            self.last_data.get('datetime') if self.last_data else None)
        if fixdt is not None:
            data['fixage'] = (now_dt - fixdt).total_seconds()
        else:
            data['fixage'] = None
        data['datetime'] = fixdt

        self.set_values(data, console_output=self.args.console_output)
        speed = data['speed']
        bearing = data['heading']
        if new_location:
            self.window.set_speedlabel(speed, bearing)
            if self.gpx is not None:
                self.gpx.update(data)
                self.recording_label_update()

        self.last_data = data
